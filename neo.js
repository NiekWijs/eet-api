const neo4j = require('neo4j-driver');

function connect(dbName) {
    this.dbName = dbName;
    this.driver = neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    )
}

function session() {
    return this.driver.session({
        database: this.dbName,
        defaultAccesMode: neo4j.WIRTE
    })
}

module.exports = {
    connect,
    session,
    dropALL: 'MATCH (n) DETACH DELETE n'
}