const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache');
const userSchema = require('./user.schema')

const UserSchema = new Schema({
    name: {
        type: String,
        require: [true, 'A user needs a name!']
    },
    roommates: {
        type: [userSchema],
        default: []
    }
})

module.exports = getModel('User', UserSchema);