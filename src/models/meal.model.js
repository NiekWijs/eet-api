const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache')

const UserSchema = require('./user.schema');

const MealSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A meal needs a name!']
    },
    capacitie: {
        type: Number,
        required: [true, 'A meal needs capacitie!']
    },
    price: {
        type: Number,
        required: [true, 'A meal needs a price!'],
        validate: {
            validator: (price) => price > 0,
            message: 'A price needs to be positive.'
        }
    },
    participants: {
        type: [UserSchema],
        default: []
    },
    cook: {
        type: UserSchema,
        required: [false, 'A meal needs a cook!']
    },
    date: {
        type: Date,
        required: [true, 'A meal needs a date!']
    }
});

module.exports = getModel('Meal', MealSchema)