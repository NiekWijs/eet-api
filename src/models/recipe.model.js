const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache');

const UserSchema = require('./user.model');
const creator = mongoose.model('creator', UserSchema);

const RecipeSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A recipe needs a name!']
    },
    ingredients: {
        type: [String],
        required: [true, 'A recipe needs ingredients!']
    },
    steps: {
        type: [String],
        required: [true, 'A recipe needs steps!']
    },
    time: {
        type: Number
    },
    private: {
        type: Boolean,
        default: true
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'creator'
    }

});

module.exports = getModel('recipe', RecipeSchema);
