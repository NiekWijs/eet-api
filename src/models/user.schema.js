const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    name: {
        type: String,
        require: [true, 'A user needs a name!']
    }
});
module.exports = userSchema;
