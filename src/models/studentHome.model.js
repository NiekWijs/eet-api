const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache');


const StudentHomeSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A studenthome needs a name!']
    },
    adress: {
        street: {
            type: String,
            required: [true, 'A adress needs a street!']
        },
        doornumber: {
            type: Number,
            required: [true, 'A adress needs a housenumber!']
        },
        addition: {
            type: String,
            required: false
        },
        zipcode: {
            type: String,
            required: [true, 'A adress needs a zipcode!'],
            unique: true
        }
    }
});
StudentHomeSchema.virtual('adress.full').get(function () {
    return this.adress.street + ' ' + this.adress.doornumber + ' ' + this.adress.addition + ' ' + this.adress.zipcode;
});


module.exports = getModel('StudentHome', StudentHomeSchema)