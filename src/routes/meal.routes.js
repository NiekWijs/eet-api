const express = require('express');
const router = express.Router();

const Meal = require('../models/meal.model')();

const CrudController = require('../controllers/crud');

const mealCrudController = new CrudController(Meal);
const mealController = require('../controllers/meal.controller');

// create a meal
router.post('/', mealCrudController.create);

// get all meals
router.get('/', mealCrudController.getAll);

// get a meal
router.get('/:id', mealCrudController.getOne)

// update a meal
router.put('/:id', mealCrudController.update);

// remove a meal
router.delete('/:id', mealCrudController.delete);

// get meals form room mates
// in prrogress requires other CRUD func's on other params
// router.get('/mealroommates/:id', mealController.)

module.exports = router;
