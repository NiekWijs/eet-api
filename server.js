// reads the .env file and stores it as environment variables, use for config
require('dotenv').config()

const connect = require('./connect')

const app = require('./src/app')

// the order of starting the app and connecting to the database does not matter
// since mongoose buffers queries till there is a connection

// start the app
const port = process.env.PORT || 80;
app.listen(port, () => {
    console.log(`server is listening on port ${port}`)
})

connect.mongo(process.env.MONGO_TEST_DB);
// connect.neo(process.env.NEO4J_PROD_DB);



