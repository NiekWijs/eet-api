const mongoose = require('mongoose')
const neo_driver = require('./neo')

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}

async function mongo(dbName) {
    try {
        await mongoose.connect(`${process.env.MONGO_URI}`, options)
        console.log(`connection to mongo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}
function neo(dbName) {
    try {
        neo_driver.connect(dbName)
        console.log(`connection to neo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}

module.exports = {
    mongo,
    neo,
}